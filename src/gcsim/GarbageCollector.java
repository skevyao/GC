package gcsim;

import java.util.Arrays;
import java.util.List;

public class GarbageCollector implements Runnable {
	private MemoryStructure<TreeData<Integer>> availableMemory;			// Reference to simulated memory space final upon initialization
	private TreeNode<TreeData<Integer>> dataStructure;					// Reference to simulated data structure final upon initialization
	//private TreeData<Integer>[] memorySnapshot;
	private boolean[] marked;
	
	public GarbageCollector(List<TreeData<Integer>> availableMemory, TreeNode<TreeData<Integer>> dataStructure) {
		this.availableMemory = (MemoryStructure) availableMemory;
		this.dataStructure = dataStructure;
		this.marked = new boolean[100];
		Arrays.fill(marked, false);
	}

	@Override
	public void run() {
		while (true) {		
			if (!availableMemory.isFull())
			{			
				try {
					// thread to sleep for 1000 milliseconds
					Thread.sleep(1000);
					System.out.println("Garbage Removing sleep() ");
				} catch (Exception e) {
					System.out.println(e);
				}
			}
			else {
				for (int i = 0; i < availableMemory.size(); i++) {
					marked[i] = !(BinarySearchTreeFactory.search(dataStructure, availableMemory.get(i)));
					System.out.println("Marking position " + i + " in Memory Structure");
				}
				
				for (int i = 0; i < availableMemory.size(); i++) {
					if (marked[i]) {
						availableMemory.remove(i);
						System.out.println("Removing position " + i + " from Memory Structure");
					}
				}
			}
		} // end of while
	}
}
