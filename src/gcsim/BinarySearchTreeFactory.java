package gcsim;

public class BinarySearchTreeFactory<T> {
	public  synchronized static <T extends Comparable<? super T>> TreeNode<T> insertBinarySearchTree(T data, TreeNode<T> treeNode) {
		
		if (treeNode.data().compareTo(data) > 0) {
			if (treeNode.left() instanceof TreeLeaf)
				treeNode.setLeft(new TreeNode<T>(data));
			else insertBinarySearchTree(data, (TreeNode<T>) (treeNode.left()));
		}
		else if (treeNode.data().compareTo(data) < 0) {
			if (treeNode.right() instanceof TreeLeaf)
				treeNode.setRight(new TreeNode<T>(data));
			else insertBinarySearchTree(data, (TreeNode<T>) (treeNode.right()));	
		}
		return treeNode;
	}
	
	public  synchronized static <T extends Comparable<? super T>> TreeNode<T> deleteFromBinarySearchTree(T data, TreeNode<T> treeNode) {
		int cmp = data.compareTo(treeNode.data());
		
		if (cmp < 0) {
			treeNode.setLeft(deleteFromBinarySearchTree(data, (TreeNode) treeNode.left()));
		}
		else if (cmp > 0) {
			treeNode.setRight(deleteFromBinarySearchTree(data, (TreeNode) treeNode.right()));
		}
		else {
			if (treeNode.left() instanceof TreeNode) {
				if (treeNode.right() instanceof TreeLeaf) return (TreeNode) (treeNode.left());
				else {	// Both left and right are TreeNodes; use successor algorithm for deletion
					TreeNode<T> right = (TreeNode) treeNode.right();
					TreeNode<T> successor = findSuccessor((TreeNode) right);
					treeNode.setData(successor.data());
					deleteFromBinarySearchTree(successor.data(), right);
				}
			} else if (treeNode.right() instanceof TreeNode) return (TreeNode) (treeNode.right());
		}
		return treeNode;
		
	}
	
	public static <T extends Comparable<? super T>> TreeNode<T> findSuccessor(TreeNode<T> treeNode) {
		if (treeNode.left() instanceof TreeLeaf) {
			return treeNode;
		} else return findSuccessor((TreeNode) treeNode.left());
	}
	
	public static <T extends Comparable<? super T>> TreeNode<T> isBinarySearchTree(TreeNode<T> treeNode) {
		return (traverseTreeNode(treeNode, null, null) ? treeNode : null);
	}
	
	private synchronized static <T extends Comparable<? super T>> Boolean traverseTreeNode(TreeNode<T> treeNode, T min, T max) {
		Boolean status = true;
		
		if (treeNode.left() instanceof TreeNode) {
			TreeNode<T> left = (TreeNode) (treeNode.left());
			if (treeNode.data().compareTo(left.data()) < 0) status = false;
			else if (min != null) {
				if (min.compareTo(treeNode.data()) > 0) status = false;
			}
			else status = traverseTreeNode(left, min, treeNode.data());
		}
		if (treeNode.right() instanceof TreeNode) {
			TreeNode<T> right = (TreeNode) (treeNode.right());
			if (treeNode.data().compareTo(right.data()) > 0) status = false;
			else if (max != null) {
				if (max.compareTo(treeNode.data()) < 0) status = false;
			}
			else status = traverseTreeNode(right, treeNode.data(), max);
		}
		return status;
	}
	
	public synchronized static <T extends Comparable<? super T>> boolean search(TreeNode<TreeData<T>> treeNode, TreeData<T> treeData) {
		boolean found = false;
	
		if (treeNode.data() != treeData) {
			if (!(treeNode.left() instanceof TreeLeaf)) found = BinarySearchTreeFactory.search((TreeNode) treeNode.left(), treeData);
			if (!(treeNode.right() instanceof TreeLeaf)) found = BinarySearchTreeFactory.search((TreeNode) treeNode.right(), treeData);
		} else {
			found = true;
		}
		
		return found;
				
	}
	
	public  synchronized static <T extends Comparable<? super T>> void flipBinarySearchTree(TreeNode<T> treeNode) {
		
		if (treeNode.right() instanceof TreeNode) {
			TreeNode<T> temp = (TreeNode) treeNode.right();
			treeNode.setRight(treeNode.left());
			treeNode.setLeft(temp);
			flipBinarySearchTree((TreeNode) treeNode.left());
			if (treeNode.right() instanceof TreeNode) flipBinarySearchTree((TreeNode) treeNode.right());
		} else if (treeNode.left() instanceof TreeNode) {
			TreeNode<T> temp = (TreeNode) treeNode.left();
			treeNode.setLeft(treeNode.right());
			treeNode.setRight(temp);
			flipBinarySearchTree((TreeNode) treeNode.right());
		}
		
	}
}
