package gcsim;

public class TreeNode<T extends Comparable<? super T>> extends TreeElem {
	private TreeElem left, right;
	private T data;
	
	public TreeElem left() {
		return this.left;
	}

	public TreeElem right() {
		return this.right;
	}
	
	public T data() {
		return this.data;
	}

	public void setLeft(TreeElem left) {
		this.left = left;
	}

	public void setRight(TreeElem right) {
		this.right = right;
	}
	
	public void setData(T data) {
		this.data = data;
	}

	public TreeNode(T data) {
		this.setLeft(new TreeLeaf());
		this.setRight(new TreeLeaf());
		this.data = data;
	}
		
	@Override
	public String toString() {
		return this.data.toString() 
				+ "\t-> left " + this.left.toString() 
				+ "\n\t-> right " + this.right.toString();
	}
/*	
	public static void main(String[] args) {
		TreeNode<TreeData<Integer>> newTree = new TreeNode<>(new TreeData<>(4, "b"));
		newTree.setLeft(new TreeNode<>(new TreeData<>(3, "a")));
		newTree.setRight(new TreeNode<>(new TreeData<>(5, "c")));
		System.out.println(newTree);
	}
*/
}
