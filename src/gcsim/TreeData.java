package gcsim;

import java.util.ArrayList;
import java.util.List;

public class TreeData<T> implements Comparable<TreeData<T>> {
	private Integer num;
	private String word;
	private List<Integer> numList = new ArrayList<>();
	private List<String> wordList = new ArrayList<>();
	
	public TreeData(Integer n, String s) {
		setNum(n);
		setWord(s);
		
		for(int i = 0; i < 10; i++) {
			numList.add(n);
		}

		for(int i = 0; i < 10; i++) {
			wordList.add(s);
		}
}
	
	public void setNum(Integer n) {
		this.num = n;
	}
	
	public void setWord(String s) {
		this.word = s;
	}
	
	public Integer retNum() {
		return this.num;
	}
	
	public String retWord() {
		return this.word;
	}

	@Override
	public String toString() {
		return "num: " + this.num.toString() 
				+ " word: " + this.word.toString() + "\n";
				//+ "numList: " + this.numList.toString() + "\n"
				//+ "wordList: " + this.wordList.toString();
	}

	@Override
	public int compareTo(TreeData<T> o) {
		if (this == o) return 0;
		if (this.num == o.retNum()) return 0;
		if (this.num < o.retNum()) return -1;
		if (this.num > o.retNum()) return 1;
		
		return 0;
	}
	
}
