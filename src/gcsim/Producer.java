package gcsim;

import java.util.List;
import java.util.Random;

public class Producer implements Runnable {
	private AllocateQueue<TreeData<Integer>> memoryAllocationQueue;
	private MemoryStructure<TreeData<Integer>> availableMemory;			// Reference to simulated memory space final upon initialization
	private TreeNode<TreeData<Integer>> dataStructure;					// Reference to simulated data structure final upon initialization
	private boolean[] numsGenerated;

	
	public Producer(AllocateQueue<TreeData<Integer>> memoryAllocationQueue, List<TreeData<Integer>> availableMemory, TreeNode<TreeData<Integer>> dataStructure, boolean[] numsGenerated) {
		this.memoryAllocationQueue = memoryAllocationQueue;
		this.availableMemory = (MemoryStructure) availableMemory;
		this.dataStructure = dataStructure;
		this.numsGenerated = numsGenerated;
	
	}
	
	@Override
	public void run() {
		Random numGen = new Random();
		while (true) {
			if (!availableMemory.isFull() ){
				int randNum = numGen.nextInt(100);
				
				synchronized(numsGenerated) {	
					TreeData<Integer> newData = new TreeData<>(randNum, "a");
					BinarySearchTreeFactory.insertBinarySearchTree(newData, dataStructure);
					numsGenerated[randNum] = true;
					try {
						 System.out.println("Producer Trying to put data structure " + randNum);
						memoryAllocationQueue.put(newData);
						//System.out.println("Producer Current contents of memory allocation queue: " + memoryAllocationQueue);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
			    } // release sync
			} // end if
			try {
				// System.out.println("Producer Sleep");
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}// end while
	}

}
