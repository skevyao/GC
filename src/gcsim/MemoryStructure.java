package gcsim;

import java.lang.ClassCastException;
import java.lang.IndexOutOfBoundsException;
import java.util.AbstractList;
import java.util.List;

public class MemoryStructure<T> extends AbstractList<T> implements List<T> {
	
	private int head;
	private int size;
	private int occupied;
	private Object[] elements;
	
	public MemoryStructure(int size) {
		elements = new Object[size];
		head = 0;
		this.size = size;
	}

	/*
	 * To access the nth element after the head, if head + n exceeds list size
	 * then offset by taking modulus
	 * 
	 * @param offset the nth element to access after the head
	 */
	private int offsetPos(int offset) {
		return (head + offset) % size;
	}

	/*
	 * Rotate the head by n elements
	 * 
	 * @param shift the number of elements to rotate the head by
	 */
	public void rotate(int shift) {
		head = offsetPos(shift);
	}
	
	private void checkIndexRange(int index) {
		if (index < 0 || index >= size)
			throw new IndexOutOfBoundsException("Index " + index + " is out of bounds");
	}

	@Override
	public T get(int index) {
		checkIndexRange(index);
		
		return (T) elements[index];
	}
	
	@Override
	public synchronized T set(int index, T elem) {
		checkIndexRange(index);
		
		T oldElem = (T) elements[index];
		elements[index] = elem;
		if (oldElem == null) occupied++;		// Keep track of number of occupied memory positions
		//System.out.println("Memory Structure now has " + occupied + " positions occupied out of " + size + " positions");
		return oldElem;
	}
	
	public  synchronized int addToNextOpenPos(T elem) {
		while (elements[head] != null) {
			System.out.println("Current element at position ");
			head = offsetPos(1);
			//System.out.println("Memory Structure current position incrementing to " + head);
		}
		
		this.set(head, elem);
		System.out.println("On set, Memory Structure at position " + head + " now has element");
		// head = offsetPos(1);
		return head;
	}
	
	@Override
	public synchronized T remove(int index) {

		checkIndexRange(index);
		T oldElem = (T) elements[index];
		elements[index] = null;
		if (oldElem != null) occupied--;
		return oldElem;
	}

	@Override
	public int size() {
		return size;
	}
	
	@Override
	public boolean isEmpty() {
		return size == 0;
	}
	
	public  boolean isFull() {
		return occupied == size;
	}
	
	@Override
	public Object[] toArray() {
		Object[] a = new Object[size];
		
		System.arraycopy(elements, 0, a, 0, size);
		
		return a;
	}
	
	@Override
	public boolean contains(Object o) {
		return indexOf(o) >= 0;
	}
	
	@Override
	public int indexOf(Object o) {
		if (o == null) {
			for (int i = 0; i < size; i++)
				if (elements[offsetPos(i)] == null)
					return i;
		} else {
			for (int i = 0; i < size; i++)
				if (o.equals(elements[offsetPos(i)]))
					return i;
		}
		return -1;
	}
}
