package gcsim;

import java.util.Arrays;
import java.util.Collections;

public class VMThread {
	private static int numAllocated;
	private static int maxAllocatable;
	
	private static AllocateQueue<TreeData<Integer>> memoryAllocationQueue;
	private static MemoryStructure<TreeData<Integer>> availableMemory;
	private static TreeNode<TreeData<Integer>> dataTree;
	private boolean[] numsGenerated;
	
	
	/*	
	public void allocateMemory(TreeNode<TreeData<Integer>> data) {
		TreeNode<TreeData<Integer>> objectToAllocate;
		
		try {
			while ((objectToAllocate = memoryAllocationQueue.take()) != null) {
				int addedToPos = availableMemory.addToNextOpenPos(data);
				System.out.println("Allocated TreeNode to memory position " + addedToPos);
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
*/
	public static void main(String[] args) {
		numAllocated = 0;
		maxAllocatable = 30;
		availableMemory = new MemoryStructure<>(maxAllocatable);
		memoryAllocationQueue = new AllocateQueue<>();
		 dataTree = new TreeNode<>(new TreeData<Integer>(50, "a"));
		final boolean[] numsGenerated = new boolean[100];
		
		
		Arrays.fill(numsGenerated, false);
		TreeData<Integer> objectToAllocate;
		
		Producer p1 = new Producer(memoryAllocationQueue, availableMemory, dataTree, numsGenerated);
		Producer p2 = new Producer(memoryAllocationQueue, availableMemory, dataTree, numsGenerated);
		Producer p3 = new Producer(memoryAllocationQueue, availableMemory, dataTree, numsGenerated);
		Consumer c1 = new Consumer(dataTree, numsGenerated);
		GarbageCollector gc = new GarbageCollector(availableMemory, dataTree);
		
		Thread p1Thread = new Thread(p1);
		Thread p2Thread = new Thread(p2);
		Thread p3Thread = new Thread(p3);
		Thread c1Thread = new Thread(c1);
		Thread gcThread = new Thread(gc);
		
		p1Thread.start();
		p2Thread.start();
		p3Thread.start();
		c1Thread.start();
		gcThread.start();

		while (true) {
			objectToAllocate = null;
			try {
				objectToAllocate = memoryAllocationQueue.take();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("VM About to allocate object " + objectToAllocate);
			synchronized(numsGenerated) {
				if (objectToAllocate != null && (!availableMemory.isFull() ))  {
					//System.out.println("VM About to allocate object " + objectToAllocate);
					int addedToPos = availableMemory.addToNextOpenPos(objectToAllocate);
					System.out.println("VM Allocated TreeNode to memory position " + addedToPos);
				}
			}  // release sync
			try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
			}
		} //end while


	}
}
