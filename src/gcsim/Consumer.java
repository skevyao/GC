package gcsim;

import java.util.Random;

public class Consumer implements Runnable {
	private TreeNode<TreeData<Integer>> dataStructure;					// Reference to simulated data structure final upon initialization
	private boolean[] numsGenerated;
	
	
	public Consumer(TreeNode<TreeData<Integer>> dataStructure, boolean[] numsGenerated) {
		this.dataStructure = dataStructure;
		this.numsGenerated = numsGenerated;
		
	}

	@Override
	public void run() {
		Random numGen = new Random();
		try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		} 	
		while (true) {
			int randNum = numGen.nextInt(100);
			synchronized(numsGenerated) {
				if (numsGenerated[randNum] == true) {
					TreeData<Integer> newData = new TreeData<>(randNum, "a");
					//System.out.println(dataStructure);
					System.out.println("Consumer Deleting element " + randNum + " from data structure");
					BinarySearchTreeFactory.deleteFromBinarySearchTree(newData, dataStructure);
					numsGenerated[randNum] = false;
				}
			}  // release sync

			try {						
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
			}
		}
	} // end while 

}
